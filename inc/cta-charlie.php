<section class="bg-red cta-charlie">
	<div class="container-fluid">
		<div class="container">
			<div class="well">
				<h2 class="heading-bottom-border">PROFESSIONAL GLASS AND METAL SOLUTIONS</h2>
			</div>
			<div class="well-a">
				
				<?php
					if (is_page( 'contact' )) {
						$charlietxt = 'Our Services';
						$charlieurl = '/services/';
					} else {
						$charlietxt = 'Get A Quote';
						$charlieurl = '/contact/';
					}
				?>
				
				<a class="btn btn-black" href="<?php echo $charlieurl; ?>"><?php echo $charlietxt; ?></a>
			</div>
		</div>
	</div>
</section>