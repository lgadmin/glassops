<section class="cta-bravo">
	<div class="container-fluid">
		<div class="container">
			<div class="well">
				<h2 class="heading-bottom-border">Contact Us</h2>
				<p>Get in touch with our team of experts today for a quote.</p>
			</div>

			<div class="well-a">
				<div class="letter"><a href="mailto:matt@glassops.ca">Matt@glassops.ca</a></div>
				<div class="phone"><a href="tel:+12504601312">250-460-1312</a></div>
			</div>
			
			<div class="well-b">
				<ul>
				 	<li><strong>Monday</strong><span>8am - 4:30pm</span></li>
				 	<li><strong>Tuesday</strong><span>8am - 4:30pm</span></li>
				 	<li><strong>Wednesday</strong><span>8am - 4:30pm</span></li>
				 	<li><strong>Thursday</strong><span>8am - 4:30pm</span></li>
				 	<li><strong>Friday</strong><span>8am - 4:30pm</span></li>
				</ul>
			</div>
		</div>
	</div>
</section>