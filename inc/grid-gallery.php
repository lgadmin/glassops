<div class="grid-gallery">
	<?php if( have_rows('gallery') ): ?>
		<?php while( have_rows('gallery') ): the_row(); ?>
			<div class="gallery-item"> 
				<div class="gallery-image">
					<a href="<?php echo get_sub_field('gallery_url'); ?>"><?php echo wp_get_attachment_image(get_sub_field('gallery_image'), 'full'); ?></a>	
				</div>
				<div class="gallery-body">
					<a href="<?php echo get_sub_field('gallery_url'); ?>"><span class="gallery-title"><?php echo get_sub_field('gallery_item_title'); ?></span></a>
					<?php if (!is_front_page()) {echo get_sub_field('gallery_item_description'); } ?>
				</div>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>


