
<!-- Default Address Stuff -->
<div class="address-card">
	
	<a id="logo" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php if(is_front_page()){?><h1><?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?></h1><?php } ?><img class="logo" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" src="<?php echo get_stylesheet_directory_uri(); ?>/images/glass-ops-reverse.png" /></a>
	
	<span class="h2">Glass Ops Glazing and Metal Solutions</span>
	
<!-- 	<address itemscope="" itemtype="http://schema.org/LocalBusiness">
		<span class="card-map-marker" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
			<span itemprop="streetAddress">12345 SHACK MILLS ST.,</span><br>
			<span itemprop="addressLocality">Vancouver</span>, <span itemprop="addressRegion">BC</span>&nbsp;<span itemprop="postalCode">V3B 0B6</span><br>
		</span>
	</address> -->

</div>

