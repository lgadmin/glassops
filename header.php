<?php
/**
 * The Header for our child theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Responsive Twenty_Ten
 * @since Responsive Twenty Ten 0.1
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header role="banner">
	<div class="container-fluid">
		<?php get_template_part("/inc/header-logo"); ?>
		<?php get_template_part("/inc/utility-bar"); ?>
		<?php get_template_part("/inc/main-nav"); ?>
	</div>
</header>

<?php if (!is_front_page()) : ?>
	<div class="featured-image">
		<div class="img-cont">
			 <img src="<?php echo get_stylesheet_directory_uri() . '/images/go-internal-headers.jpg'; ?>">
		</div>		
		<div class="title-cont">
			<span class="featured-title"><?php echo get_the_title(); ?></span>
		</div>

	</div>
<?php endif; ?>