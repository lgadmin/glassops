<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<footer role="contentinfo">
	
	<div class="bg-blackalt1 glass-footer">
		<div class="container-fluid">
			<section class="site-info">
				<div class="addresscard"> &nbsp; <?php  get_template_part("/inc/address-card"); ?></div>
				<div class="footer-logos">
					<?php if(have_rows('footer_logos', 'option')) : ?>
						<?php while(have_rows('footer_logos', 'option')) : the_row(); ?>
							<?php 
								$logo_image = get_sub_field('footer_logo_image','option');
								$logo_link = get_sub_field('footer_logo_link','option');
								$logo_link_url = $logo_link['url'];
								$logo_link_target = $logo_link['target'] ? $logo_link['target'] : '_self';
							 ?>
							<a href="<?php echo esc_url($logo_link_url); ?>" target="<?php echo esc_attr($logo_link_target); ?>"><img src="<?php echo $logo_image['url']; ?>" alt="<?php echo $logo_image['alt']; ?>"></a>
						<?php endwhile; ?>
					 <?php endif;?>
				</div>
				<div class="footernav"><?php get_template_part("/inc/nav-footer"); ?></div>
			</section>
		</div>
	</div>

	<div class="bg-black glass-legal">
		<div class="container-fluid">

				<?php dynamic_sidebar( 'footer-widget-area' ); ?>

				<div id="copyright">  
				
					<?php
						get_sidebar( 'footer' );
						$date = getdate();
						$year = $date['year']; 
					?>

					<a href="<?php echo home_url( '/' ); ?>">Copyright</a> &copy;  <a href="<?php echo("$year"); ?><?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> | <a href="/privacy-policy">Privacy Policy</a>
				</div>

				<div id="longevity">
					<a target="_blank" href="http://www.longevitygraphics.com">Website Design</a> by <a target="_blank" href="http://www.longevitygraphics.com">Longevity Graphics</a>
				</div>
		
		</div> 
	</div>

</footer>

<?php wp_footer(); ?>
</body>
</html>