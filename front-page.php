<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div class="container-fluid home-slider">
	<?php echo do_shortcode("[masterslider id='5']"); ?>
	<?php echo do_shortcode('[gravityform id="1" title="true" description="false"]'); ?>
</div>

<?php get_template_part("/inc/cta-alpha"); ?>

<div class="bg-pattern">
	<section class="content container">
		<main id="content" role="main" class="one-column">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'page' );
			?>

			<?php get_template_part("/inc/grid-gallery"); ?>

		</main>
	</section>
</div>

<?php get_template_part("/inc/cta-bravo"); ?>

<?php get_footer(); ?>
